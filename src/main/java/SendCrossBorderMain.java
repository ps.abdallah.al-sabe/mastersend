package com.mastercard.test;

import com.mastercard.developer.interceptors.OkHttp2OAuth1Interceptor;
import com.mastercard.developer.utils.AuthenticationUtils;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.openapitools.client.ApiClient;
import org.openapitools.client.api.CardedRateApi;
import org.openapitools.client.api.PaymentApi;
import org.openapitools.client.api.RetrievePaymentApi;
import org.openapitools.client.model.*;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;

public class SendCrossBorderMain {

    public static void main(String[] args) throws Exception {

        ApiClient client = new ApiClient();

        String consumerKey = "O6Eje6t0VNGinwLvtXGbtkf6jf579Wtd1itlkZED840b2dcc!96078540c2854bde840d39ad17f4fe870000000000000000";
        String signingKeyFilePath = "/home/u719/IdeaProjects/mastertest/src/main/resources/keyalias-sandbox.p12";
        String signingKeyAlias = "keyalias";
        String signingKeyPassword = "P@ssw0rd";
        PrivateKey signingKey = AuthenticationUtils.loadSigningKey(signingKeyFilePath, signingKeyAlias, signingKeyPassword);

        client.setBasePath("https://sandbox.api.mastercard.com");
        client.setDebugging(true);
        client.setReadTimeout(40000);
        List<Interceptor> interceptors = client.getHttpClient().networkInterceptors();
        interceptors.add(new ForceJsonResponseInterceptor());
        interceptors.add(new OkHttp2OAuth1Interceptor(consumerKey, signingKey));

        String partnerId = "SANDBOX_1234567";  // example: "BEL_MASEND5ged2"

        /*Payment API call*/
        PaymentApi paymentApi = new PaymentApi(client);
        System.out.println("Calling Make Payment");
        PaymentRequestWrapper paymentRequestWrapper = getPaymentRequest(); // Populate payment request
        PaymentWrapper remitResponse = paymentApi.payment(partnerId,paymentRequestWrapper);
        System.out.println("Make payment response : " + remitResponse);

        /*Retrieve Payment API call*/
        RetrievePaymentApi retrievePayment = new RetrievePaymentApi(client);
        System.out.println("Calling Retrieve Payment");
        String ref = "061574074811229"; //Can be obtained by remitResponse.getPayment().getTransactionReference();
        RetrievePaymentWrapper response = retrievePayment.transactionStatus(partnerId, ref);
        System.out.println("Retrieve Payment response: " +response);
    }

    private static PaymentRequestWrapper getPaymentRequest() {

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setTransactionReference(String.valueOf(System.currentTimeMillis()));
        paymentRequest.setSenderAccountUri("tel:+254108989");
        paymentRequest.setRecipientAccountUri("ewallet:paypal_user011");
        /* Amount information */
        PaymentAmount amount = new PaymentAmount();
        amount.setAmount("200");
        amount.setCurrency("INR");
        paymentRequest.setPaymentAmount(amount);
        paymentRequest.setPaymentOriginationCountry("ARE");

        FxType quoteType = new FxType();
        Reverse reverseFees = new Reverse();
        reverseFees.setSenderCurrency("USD");
        quoteType.setReverse(reverseFees);
        paymentRequest.setFxType(quoteType);
        paymentRequest.setBankCode("NP021");
        paymentRequest.setPaymentType("P2B");

        /*Sender Information */
        Sender senderData = new Sender();
        senderData.setFirstName("Pat");
        senderData.setLastName("Rose");
        senderData.setNationality("IND");
        Address senderAddress = new Address();
        senderAddress.setLine1("53 Main Street");
        senderAddress.setLine2("5A");
        senderAddress.setCity("Pune");
        senderAddress.setCountrySubdivision("MH");
        senderAddress.setCountry("IND");
        senderAddress.setPostalCode("411001");
        senderData.setAddress( senderAddress);

        GovernmentIds idData1 = new GovernmentIds();
        List<GovernmentIds> governmentIds = new ArrayList<>();
        idData1.setGovernmentIdUri("ppn:123456789;expiration-date=2019-05-27;issue-date=2011-07-12;country=USA");
        governmentIds.add(idData1);
        senderData.setGovernmentIds(governmentIds);
        senderData.setDateOfBirth("1985-06-24");
        paymentRequest.setSender(senderData);

        /*Recipient information */
        Recipient recipientData = new Recipient();
        recipientData.setEmail("test@gmail.com");
        recipientData.setNationality("USA");
        recipientData.setOrganizationName("WU");
        Address recipientAddress = new Address();
        recipientAddress.setLine1("123 MainStreet");
        recipientAddress.setLine2("5A");
        recipientAddress.setCity("Arlington");
        recipientAddress.setCountrySubdivision("VA");
        recipientAddress.setCountry("USA");
        recipientAddress.setPostalCode("22207");
        recipientData.setAddress(recipientAddress);
        paymentRequest.setRecipient(recipientData);

        /* Additional Data */
        List <DataField> fields = new ArrayList<>();
        DataField dataField1 = new DataField();
        dataField1.setName("501");dataField1.setValue("1234222222");
        DataField dataField2 = new DataField();
        dataField2.setName("503");dataField2.setValue("12362");
        AdditionalData additionalField = new AdditionalData();
        additionalField.setDataField(fields);
        paymentRequest.setAdditionalData(additionalField);

        paymentRequest.setSourceOfIncome("Bank");
        paymentRequest.setReceivingBankName("Royal Exchange");
        paymentRequest.setReceivingBankBranchName("Quad Cities");
        paymentRequest.setPaymentFileIdentifier("1233241223");
        PaymentRequestWrapper wrapper = new PaymentRequestWrapper();
        wrapper.setPaymentrequest(paymentRequest);
        return wrapper;
    }

    /**Add "Format=JSON" to the request for the service/gateway to return a JSON response.*/
    private static class ForceJsonResponseInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            String withJsonFormatUrl = withJsonFormat(originalRequest.uri().toString());
            Request newRequest = originalRequest.newBuilder().url(withJsonFormatUrl).build();
            return chain.proceed(newRequest);
        }

        private String withJsonFormat(String uri) {
            StringBuilder newUri = new StringBuilder(uri);
            newUri.append(uri.contains("?") ? "&" : "?");
            newUri.append("Format=JSON");
            return newUri.toString();
        }
    }
}


