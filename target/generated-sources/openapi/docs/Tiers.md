
# Tiers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tier** | [**List&lt;Tier&gt;**](Tier.md) | See child attributes |  [optional]



