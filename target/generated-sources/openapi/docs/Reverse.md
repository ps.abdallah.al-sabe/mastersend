
# Reverse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**senderCurrency** | **String** | This parameter contains the three-letter \&quot;ISO 4217\&quot; currency code in which the sender will send the payment. This parameter is required for reverse quotes.  Note that if no quote_type is used then the quote defaults to forward with the fees_included field set to “true.”  A forward quote and a reverse quote shouldn’t be used together. Using quoterequest.quote_type.forward.fees_included with quoterequest.quote_type.reverse.sender_currency throws an error.    It’s an optional parameter that holds alphabet characters with an exact length of three |  [optional]



