
# Forward

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feesIncluded** | **String** | This parameter indicates how payment fees will be paid. If true, fees are subtracted from the sender’s amount. If false, then the sender will pay fees in addition to the sender’s amount. Note that if no quote_type is used then the quote defaults to forward with the fees_included field set to “true.” A forward quote and a reverse quote shouldn’t be used together. Using quoterequest.quote_type.forward.fees_included with quoterequest.quote_type.reverse.sender_currency throws an error.  It’s an optional parameter that holds a Boolean value. Valid values are “true” or “false”.  |  [optional]
**receiverCurrency** | **String** | This parameter contains the three-letter \&quot;ISO 4217\&quot; currency code of the account to receive the funds.  This parameter is required for forward quotes where the beneficiary Account uri &#x3D; pan.  It’s an optional parameter that holds  alphabet characters with an exact length of three.  |  [optional]



