
# Rates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate** | [**List&lt;Rate&gt;**](Rate.md) | See child attributes |  [optional]



