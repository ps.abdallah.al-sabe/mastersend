
# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventRef** | **String** | This parameter contains the unique event reference number. [A-Za-z0-9_-]+ |  [optional]
**eventType** | **String** | This parameter holds information related to an escalated FX rate or a standard published FX rate.  Valid values:  • CARDFX_PUB - A new FX rate published as part of its scheduled standard publication.  • CARDFX_ESC - One or more FX rates have been escalated resulting in an out of Schedule Push. Only updated FX rates are provided.  Note: Out of Schedule push rates are sent on rare occasions when a typical market fluctuations exist.  |  [optional]
**rates** | [**Rates**](Rates.md) |  |  [optional]



