
# FxRates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate** | [**List&lt;FxRate&gt;**](FxRate.md) | See child attributes |  [optional]



