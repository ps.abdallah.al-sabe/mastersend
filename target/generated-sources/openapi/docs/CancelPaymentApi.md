# CancelPaymentApi

All URIs are relative to *https://api.mastercard.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelRequest**](CancelPaymentApi.md#cancelRequest) | **POST** /send/v1/partners/{partner-id}/crossborder/{payment-id}/cancel | This resource is ONLY usable for cash-out and some mobile money providers. For those scenarios, it only allows a cancel request for a transaction that is in a PENDING status.    NOTE:  This functionality does not work for bank account channels. For the sample code to run successfully, please use a Payment Id of a transaction that is in PENDING status and avoid reusing same payment Id for multiple cancellations. 


<a name="cancelRequest"></a>
# **cancelRequest**
> CancelPaymentWrapper cancelRequest(partnerId, paymentId, cancelPaymentRequestWrapper)

This resource is ONLY usable for cash-out and some mobile money providers. For those scenarios, it only allows a cancel request for a transaction that is in a PENDING status.    NOTE:  This functionality does not work for bank account channels. For the sample code to run successfully, please use a Payment Id of a transaction that is in PENDING status and avoid reusing same payment Id for multiple cancellations. 

This resource is ONLY usable for cash-out and some mobile money providers. For those scenarios, it only allows a cancel request for a transaction that is in a PENDING status.    NOTE:  This functionality does not work for bank account channels. For the sample code to run successfully, please use a Payment Id of a transaction that is in PENDING status and avoid reusing same payment Id for multiple cancellations. 

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.CancelPaymentApi;


CancelPaymentApi apiInstance = new CancelPaymentApi();
String partnerId = BEL_MASEND5ged2; // String | Path Param - This is provider assigned client id. Details - string, 35
String paymentId = 0999999034810154901; // String | Path Param - This is the id of the payment to cancel.
CancelPaymentRequestWrapper cancelPaymentRequestWrapper = new CancelPaymentRequestWrapper(); // CancelPaymentRequestWrapper | Contains the details of the request message.
try {
    CancelPaymentWrapper result = apiInstance.cancelRequest(partnerId, paymentId, cancelPaymentRequestWrapper);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CancelPaymentApi#cancelRequest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partnerId** | **String**| Path Param - This is provider assigned client id. Details - string, 35 |
 **paymentId** | **String**| Path Param - This is the id of the payment to cancel. |
 **cancelPaymentRequestWrapper** | [**CancelPaymentRequestWrapper**](CancelPaymentRequestWrapper.md)| Contains the details of the request message. | [optional]

### Return type

[**CancelPaymentWrapper**](CancelPaymentWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

