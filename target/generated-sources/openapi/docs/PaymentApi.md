# PaymentApi

All URIs are relative to *https://api.mastercard.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payment**](PaymentApi.md#payment) | **POST** /send/v1/partners/{partner-id}/crossborder/payment | This resource is used to initiate and submit a cross-border payment. There are two options available:  Option #1: Payment with a quote - This option allows associating the FX rate and amounts calculated for a previously submitted quote to the payment by including the proposal Id of the quote in the payment request.  For these payments, the fields filled in the quote do not need to be provided in the payment itself. Submitting a successful Quote API call will generate a proposal Id.     NOTE: For the test to run successfully, please use the information (proposal Id and transaction_reference) from a unique quote submission using Quote API on each run.  Option #2: Payment without a quote - In this option, the proposal ID is left blank and all required fields for a payment must be provided in the request.  For these payments, the FX rate that is valid at the time the payment is submitted, will be applied to the payment and all of the calculated amounts provided in the payment response will be based on this FX rate.  NOTE: For the test to run successfully, please use an unique transaction_reference on each run.


<a name="payment"></a>
# **payment**
> PaymentWrapper payment(partnerId, paymentRequestWrapper)

This resource is used to initiate and submit a cross-border payment. There are two options available:  Option #1: Payment with a quote - This option allows associating the FX rate and amounts calculated for a previously submitted quote to the payment by including the proposal Id of the quote in the payment request.  For these payments, the fields filled in the quote do not need to be provided in the payment itself. Submitting a successful Quote API call will generate a proposal Id.     NOTE: For the test to run successfully, please use the information (proposal Id and transaction_reference) from a unique quote submission using Quote API on each run.  Option #2: Payment without a quote - In this option, the proposal ID is left blank and all required fields for a payment must be provided in the request.  For these payments, the FX rate that is valid at the time the payment is submitted, will be applied to the payment and all of the calculated amounts provided in the payment response will be based on this FX rate.  NOTE: For the test to run successfully, please use an unique transaction_reference on each run.

This resource is used to initiate and submit a cross-border payment. There are two options available:  Option #1: Payment with a quote - This option allows associating the FX rate and amounts calculated for a previously submitted quote to the payment by including the proposal Id of the quote in the payment request.  For these payments, the fields filled in the quote do not need to be provided in the payment itself. Submitting a successful Quote API call will generate a proposal Id.     NOTE: For the test to run successfully, please use the information (proposal Id and transaction_reference) from a unique quote submission using Quote API on each run.  Option #2: Payment without a quote - In this option, the proposal ID is left blank and all required fields for a payment must be provided in the request.  For these payments, the FX rate that is valid at the time the payment is submitted, will be applied to the payment and all of the calculated amounts provided in the payment response will be based on this FX rate.  NOTE: For the test to run successfully, please use an unique transaction_reference on each run.

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.PaymentApi;


PaymentApi apiInstance = new PaymentApi();
String partnerId = BEL_MASEND5ged2; // String | This is a path parameter that accepts the provider assigned identification.   It holds a string of alphanumeric special characters with an exact length of 35.  
PaymentRequestWrapper paymentRequestWrapper = new PaymentRequestWrapper(); // PaymentRequestWrapper | Contains the details of the request message.
try {
    PaymentWrapper result = apiInstance.payment(partnerId, paymentRequestWrapper);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentApi#payment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partnerId** | **String**| This is a path parameter that accepts the provider assigned identification.   It holds a string of alphanumeric special characters with an exact length of 35.   |
 **paymentRequestWrapper** | [**PaymentRequestWrapper**](PaymentRequestWrapper.md)| Contains the details of the request message. | [optional]

### Return type

[**PaymentWrapper**](PaymentWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

