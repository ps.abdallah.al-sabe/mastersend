# QuotesApi

All URIs are relative to *https://api.mastercard.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**quotes**](QuotesApi.md#quotes) | **POST** /send/v1/partners/{partner-id}/crossborder/quotes | This resource provides information on a cross-border payment before it is initiated and submitted.  The information provided includes the amount to be credited to the beneficiary, the amount to charge the sender, the Origination Institution settlement amount, the FX rate, and more.   NOTE: For the test to run correctly, please use an unique transaction_reference on each run.


<a name="quotes"></a>
# **quotes**
> QuoteWrapper quotes(partnerId, quoteRequestWrapper)

This resource provides information on a cross-border payment before it is initiated and submitted.  The information provided includes the amount to be credited to the beneficiary, the amount to charge the sender, the Origination Institution settlement amount, the FX rate, and more.   NOTE: For the test to run correctly, please use an unique transaction_reference on each run.

This resource provides information on a cross-border payment before it is initiated and submitted.  The information provided includes the amount to be credited to the beneficiary, the amount to charge the sender, the Origination Institution settlement amount, the FX rate, and more.   NOTE: For the test to run correctly, please use an unique transaction_reference on each run.

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.QuotesApi;


QuotesApi apiInstance = new QuotesApi();
String partnerId = BEL_MASEND5ged2; // String | This is a path parameter that accepts the provider assigned identification.   It holds a string of alphanumeric special characters with an exact length of 35.  
QuoteRequestWrapper quoteRequestWrapper = new QuoteRequestWrapper(); // QuoteRequestWrapper | Contains the details of the request message.
try {
    QuoteWrapper result = apiInstance.quotes(partnerId, quoteRequestWrapper);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling QuotesApi#quotes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partnerId** | **String**| This is a path parameter that accepts the provider assigned identification.   It holds a string of alphanumeric special characters with an exact length of 35.   |
 **quoteRequestWrapper** | [**QuoteRequestWrapper**](QuoteRequestWrapper.md)| Contains the details of the request message. | [optional]

### Return type

[**QuoteWrapper**](QuoteWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml, application/json
 - **Accept**: application/xml, application/json

