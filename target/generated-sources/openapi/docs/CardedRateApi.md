# CardedRateApi

All URIs are relative to *https://api.mastercard.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cardedPUSH**](CardedRateApi.md#cardedPUSH) | **POST** /callbackpath | The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  
[**getFxRates**](CardedRateApi.md#getFxRates) | **GET** /send/v1/partners/{partner-id}/crossborder/rates | The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.


<a name="cardedPUSH"></a>
# **cardedPUSH**
> StatusWrapper cardedPUSH(eventWrapper)

The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  

The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.CardedRateApi;


CardedRateApi apiInstance = new CardedRateApi();
EventWrapper eventWrapper = new EventWrapper(); // EventWrapper | See child attributes
try {
    StatusWrapper result = apiInstance.cardedPUSH(eventWrapper);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CardedRateApi#cardedPUSH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventWrapper** | [**EventWrapper**](EventWrapper.md)| See child attributes | [optional]

### Return type

[**StatusWrapper**](StatusWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFxRates"></a>
# **getFxRates**
> RatesWrapper getFxRates(partnerId)

The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.

The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.CardedRateApi;


CardedRateApi apiInstance = new CardedRateApi();
String partnerId = BEL_MASEND5ged2; // String | Path Param - This is the Assigned Provider Id. Details- This parameter holds a string of alphanumeric special characters with a max length of 35.
try {
    RatesWrapper result = apiInstance.getFxRates(partnerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CardedRateApi#getFxRates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partnerId** | **String**| Path Param - This is the Assigned Provider Id. Details- This parameter holds a string of alphanumeric special characters with a max length of 35. |

### Return type

[**RatesWrapper**](RatesWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

