
# FeeAmount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **String** | This parameter contains the currency of the Fee Amount. It holds a string of alphabet characters with an exact length of three. |  [optional]
**amount** | **String** | This parameter contains the amount of the Fee.  It holds a decimal value with a maximum length of 30 and a minimum length of 1. |  [optional]



