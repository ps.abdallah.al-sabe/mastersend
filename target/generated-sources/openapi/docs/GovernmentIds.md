
# GovernmentIds

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**governmentIdUri** | **String** | This is the URI describing the government issued identification for the sending consumer or organization.   It is a conditional parameter that holds alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the “Government ID URIs” section on this page for details about scheme specific data.  |  [optional]



