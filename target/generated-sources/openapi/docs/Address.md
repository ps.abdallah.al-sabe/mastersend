
# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line1** | **String** | This parameter contains the value in address line 1 of the person. For transactions where it is not a person, this is address line 1 for the business/entity.  It holds a string of alphanumeric special characters with a maximum length of 500 and a minimum length of 1. | 
**line2** | **String** | This parameter contains the value in address line 2 of the person. For transactions where it is not a person, this is address line 2 for the business/entity.  It holds a string of alphanumeric special characters with a maximum length of 500 and a minimum length of 1 |  [optional]
**city** | **String** | This parameter contains the city where the person resides. For transactions where it is not a person, this is the city where the business/entity resides..   It holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.  |  [optional]
**countrySubdivision** | **String** | This parameter contains the state/province of the person. For transactions where it is a Business/Entity, this is the state/province where the business/entity resides. This parameter holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 2.  For Countries that do not have a country subdivision, this field should not be provided in the payment request.  |  [optional]
**postalCode** | **String** | This parameter contains the postal code of the person. For transactions where it is not a person, this is the postal code of the Business/Entity. This parameter is only required for countries that use postal codes.  This parameter holds a string of alphanumeric special characters with a maximum length of 16 and a minimum length of 1. |  [optional]
**country** | **String** | This parameter contains the three-letter \&quot;ISO 3166-1 alpha-3\&quot; country code of the person. When it is not a person, this is the country of the business/entity.  This parameter holds a string of alphabet characters with an exact length of three. | 



