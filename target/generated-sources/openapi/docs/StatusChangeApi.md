# StatusChangeApi

All URIs are relative to *https://api.mastercard.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**statusChangePUSH**](StatusChangeApi.md#statusChangePUSH) | **POST** /webhook | The Status Change API will provide near real-time payment status updates to those OIs who opt-in for the service.  


<a name="statusChangePUSH"></a>
# **statusChangePUSH**
> StatusWrapper statusChangePUSH(notificationWrapper)

The Status Change API will provide near real-time payment status updates to those OIs who opt-in for the service.  

The Status Change API will provide near real-time payment status updates to those OIs who opt-in for the service.  

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.StatusChangeApi;


StatusChangeApi apiInstance = new StatusChangeApi();
NotificationWrapper notificationWrapper = new NotificationWrapper(); // NotificationWrapper | Contains all the fields of status change.
try {
    StatusWrapper result = apiInstance.statusChangePUSH(notificationWrapper);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusChangeApi#statusChangePUSH");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notificationWrapper** | [**NotificationWrapper**](NotificationWrapper.md)| Contains all the fields of status change. | [optional]

### Return type

[**StatusWrapper**](StatusWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

