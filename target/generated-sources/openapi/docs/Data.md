
# Data

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataField** | [**List&lt;DataField&gt;**](DataField.md) | A name/value pair which represents a data field |  [optional]



