
# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reasonCode** | **String** | This field will contain the value of Error code provided by Mastercard for \&quot;Rejected\&quot; and \&quot;Service Error\&quot; transactions. |  [optional]
**description** | **String** | This field will contain the value of Error Description related to the Error Code available in previous field. |  [optional]



