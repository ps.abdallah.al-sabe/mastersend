# RetrievePaymentApi

All URIs are relative to *https://api.mastercard.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPayment**](RetrievePaymentApi.md#getPayment) | **GET** /send/v1/partners/{partner-id}/crossborder/{payment-id} | This resource is used to retrieve the Payment transaction using the payment ID. When using the Retrieve Payment API resource to check the status of a PENDING payment, it should be used no more than every 30 minutes for each payment being retrieved.    NOTE: For the test to run successfully, please use the payment Id of an existing transaction that was created using the Payment API.
[**transactionStatus**](RetrievePaymentApi.md#transactionStatus) | **GET** /send/v1/partners/{partner-id}/crossborder | This resource is used to retrieve the Payment transaction using the transaction_reference. When using the Retrieve Payment API resource to check the status of a PENDING payment, it should be used no more than every 30 minutes for each payment being retrieved.    NOTE: For the test to run successfully, please use the transaction_reference of an existing transaction that was created using the Payment API.


<a name="getPayment"></a>
# **getPayment**
> RetrievePaymentWrapper getPayment(partnerId, paymentId)

This resource is used to retrieve the Payment transaction using the payment ID. When using the Retrieve Payment API resource to check the status of a PENDING payment, it should be used no more than every 30 minutes for each payment being retrieved.    NOTE: For the test to run successfully, please use the payment Id of an existing transaction that was created using the Payment API.

This resource is used to retrieve the Payment transaction using the payment ID. When using the Retrieve Payment API resource to check the status of a PENDING payment, it should be used no more than every 30 minutes for each payment being retrieved.    NOTE: For the test to run successfully, please use the payment Id of an existing transaction that was created using the Payment API.

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.RetrievePaymentApi;


RetrievePaymentApi apiInstance = new RetrievePaymentApi();
String partnerId = BEL_MASEND5ged2; // String | This is a path parameter that accepts the provider assigned identification.  It holds a string of alphanumeric special characters with an exact length of 35.
String paymentId = 0999999034810154901; // String | This is a path parameter that accepts the system generated unique payment identifier.  It holds a string of alphanumeric special characters with a maximum length of 31 and a minimum length of 1.
try {
    RetrievePaymentWrapper result = apiInstance.getPayment(partnerId, paymentId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RetrievePaymentApi#getPayment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partnerId** | **String**| This is a path parameter that accepts the provider assigned identification.  It holds a string of alphanumeric special characters with an exact length of 35. |
 **paymentId** | **String**| This is a path parameter that accepts the system generated unique payment identifier.  It holds a string of alphanumeric special characters with a maximum length of 31 and a minimum length of 1. |

### Return type

[**RetrievePaymentWrapper**](RetrievePaymentWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

<a name="transactionStatus"></a>
# **transactionStatus**
> RetrievePaymentWrapper transactionStatus(partnerId, ref)

This resource is used to retrieve the Payment transaction using the transaction_reference. When using the Retrieve Payment API resource to check the status of a PENDING payment, it should be used no more than every 30 minutes for each payment being retrieved.    NOTE: For the test to run successfully, please use the transaction_reference of an existing transaction that was created using the Payment API.

This resource is used to retrieve the Payment transaction using the transaction_reference. When using the Retrieve Payment API resource to check the status of a PENDING payment, it should be used no more than every 30 minutes for each payment being retrieved.    NOTE: For the test to run successfully, please use the transaction_reference of an existing transaction that was created using the Payment API.

### Example
```java
// Import classes:
//import org.openapitools.client.ApiException;
//import org.openapitools.client.api.RetrievePaymentApi;


RetrievePaymentApi apiInstance = new RetrievePaymentApi();
String partnerId = BEL_MASEND5ged2; // String | This is a path parameter that accepts the provider assigned identification.  It holds a string of alphanumeric special characters with an exact length of 35.
String ref = 0999999034810154901; // String | This is a path parameter that accepts the client-provided unique reference number of the transaction. The string held must be unique per transaction. Note that when utilizing a quote, this parameter’s value must be identical to the value created for the associated quote.  It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
try {
    RetrievePaymentWrapper result = apiInstance.transactionStatus(partnerId, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RetrievePaymentApi#transactionStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partnerId** | **String**| This is a path parameter that accepts the provider assigned identification.  It holds a string of alphanumeric special characters with an exact length of 35. |
 **ref** | **String**| This is a path parameter that accepts the client-provided unique reference number of the transaction. The string held must be unique per transaction. Note that when utilizing a quote, this parameter’s value must be identical to the value created for the associated quote.  It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1. |

### Return type

[**RetrievePaymentWrapper**](RetrievePaymentWrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

