
# StatusWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**htTPStatusCodes** | **String** | OI must respond with standard http response codes. 200 is for success and remaining codes for failures.  If failure code received, MC will retry the push for 3 times within a 30 seconds interval.  |  [optional]



