
# RejectedStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  This is the numeric code representation of the why the payment request was rejected. Valid values can be found in the “Error Code” column of the “Error Codes” section of this documentation.   It holds a string of numeric characters with an exact length of 6. |  [optional]
**message** | **String** |  This is the English text representation of the why the payment request was rejected. Valid values can be found in the “Error Description” column of the “Error Codes” section of this documentation.   It holds a string of alphanumeric special characters with a maximum length of 1000 and a minimum length of 1. |  [optional]



