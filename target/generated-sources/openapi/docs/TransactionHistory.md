
# TransactionHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notificationId** | **String** | This parameter contains the unique event reference number. [A-Za-z0-9_-]+ |  [optional]
**status** | **String** | Status of transaction.   Valid values: Success, Rejected, Pending  For transactions on the Daily Transaction Report, Current Transaction Status will be equal to the Original Transaction Status.  [alpha, max length: 100] Example: Rejected  |  [optional]
**pendingStage** | **String** | An identifier showing the transaction&#39;s pending stage. Associated with the Transaction Status. Only returned if the status is PENDING.   Valid Values: Processing; InProgress; AcceptedInProgress; RejectedInProgress; WaitingForSenderInput; AmendRequested; InProgress; (any other value)* *A compliant client MUST be able to accept any other value here. Even if such a value can’t be explicitly understood, the sending system must consider it as if it was a “InProgress” stage.   Example: InProgress  |  [optional]
**timestamp** | **String** | Timestamp of the current status.  Format:  YYYY-MM-DDTHH:MM:SS±hh[:mm]  or  YYYY-MM-DDTHH:MM:SSZ  example:  2015-03-18T14:18:55-05:00  |  [optional]



