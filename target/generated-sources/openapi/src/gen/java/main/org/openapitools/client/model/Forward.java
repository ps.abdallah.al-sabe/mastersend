/*
 * Crossborder
 * Send cross-border APIs.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apisupport@mastercard.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Forward
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-05-09T06:16:48.801+03:00[Asia/Kuwait]")
public class Forward {
  public static final String SERIALIZED_NAME_FEES_INCLUDED = "fees_included";
  @SerializedName(SERIALIZED_NAME_FEES_INCLUDED)
  private String feesIncluded;

  public static final String SERIALIZED_NAME_RECEIVER_CURRENCY = "receiver_currency";
  @SerializedName(SERIALIZED_NAME_RECEIVER_CURRENCY)
  private String receiverCurrency;

  public Forward feesIncluded(String feesIncluded) {
    this.feesIncluded = feesIncluded;
    return this;
  }

   /**
   * This parameter indicates how payment fees will be paid. If true, fees are subtracted from the sender’s amount. If false, then the sender will pay fees in addition to the sender’s amount. Note that if no quote_type is used then the quote defaults to forward with the fees_included field set to “true.” A forward quote and a reverse quote shouldn’t be used together. Using quoterequest.quote_type.forward.fees_included with quoterequest.quote_type.reverse.sender_currency throws an error.  It’s an optional parameter that holds a Boolean value. Valid values are “true” or “false”. 
   * @return feesIncluded
  **/
  @ApiModelProperty(example = "true", value = "This parameter indicates how payment fees will be paid. If true, fees are subtracted from the sender’s amount. If false, then the sender will pay fees in addition to the sender’s amount. Note that if no quote_type is used then the quote defaults to forward with the fees_included field set to “true.” A forward quote and a reverse quote shouldn’t be used together. Using quoterequest.quote_type.forward.fees_included with quoterequest.quote_type.reverse.sender_currency throws an error.  It’s an optional parameter that holds a Boolean value. Valid values are “true” or “false”. ")
  public String getFeesIncluded() {
    return feesIncluded;
  }

  public void setFeesIncluded(String feesIncluded) {
    this.feesIncluded = feesIncluded;
  }

  public Forward receiverCurrency(String receiverCurrency) {
    this.receiverCurrency = receiverCurrency;
    return this;
  }

   /**
   * This parameter contains the three-letter \&quot;ISO 4217\&quot; currency code of the account to receive the funds.  This parameter is required for forward quotes where the beneficiary Account uri &#x3D; pan.  It’s an optional parameter that holds  alphabet characters with an exact length of three. 
   * @return receiverCurrency
  **/
  @ApiModelProperty(example = "USD", value = "This parameter contains the three-letter \"ISO 4217\" currency code of the account to receive the funds.  This parameter is required for forward quotes where the beneficiary Account uri = pan.  It’s an optional parameter that holds  alphabet characters with an exact length of three. ")
  public String getReceiverCurrency() {
    return receiverCurrency;
  }

  public void setReceiverCurrency(String receiverCurrency) {
    this.receiverCurrency = receiverCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Forward forward = (Forward) o;
    return Objects.equals(this.feesIncluded, forward.feesIncluded) &&
        Objects.equals(this.receiverCurrency, forward.receiverCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(feesIncluded, receiverCurrency);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Forward {\n");
    
    sb.append("    feesIncluded: ").append(toIndentedString(feesIncluded)).append("\n");
    sb.append("    receiverCurrency: ").append(toIndentedString(receiverCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

