/*
 * Crossborder
 * Send cross-border APIs.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apisupport@mastercard.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.openapitools.client.model.Data;

/**
 * This parameter contains the list of name/value pairs containing additional parameter values. It is a conditional parameter where there can be zero to many fields sent in the payload. The number of fields, and when they are required, is based on the specific RSP and transaction type.
 */
@ApiModel(description = "This parameter contains the list of name/value pairs containing additional parameter values. It is a conditional parameter where there can be zero to many fields sent in the payload. The number of fields, and when they are required, is based on the specific RSP and transaction type.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-05-09T06:16:48.801+03:00[Asia/Kuwait]")
public class AdditionalDataList {
  public static final String SERIALIZED_NAME_RESOURCE_TYPE = "resource_type";
  @SerializedName(SERIALIZED_NAME_RESOURCE_TYPE)
  private String resourceType;

  public static final String SERIALIZED_NAME_ITEM_COUNT = "item_count";
  @SerializedName(SERIALIZED_NAME_ITEM_COUNT)
  private String itemCount;

  public static final String SERIALIZED_NAME_DATA = "data";
  @SerializedName(SERIALIZED_NAME_DATA)
  private Data data = null;

  public AdditionalDataList resourceType(String resourceType) {
    this.resourceType = resourceType;
    return this;
  }

   /**
   * This parameter contains the type of additional data list resource type.  Note that it is provided if additional_data_list is populated.  It is conditional and will only hold a string with the value \&quot;list\&quot;. 
   * @return resourceType
  **/
  @ApiModelProperty(example = "list", value = "This parameter contains the type of additional data list resource type.  Note that it is provided if additional_data_list is populated.  It is conditional and will only hold a string with the value \"list\". ")
  public String getResourceType() {
    return resourceType;
  }

  public void setResourceType(String resourceType) {
    this.resourceType = resourceType;
  }

  public AdditionalDataList itemCount(String itemCount) {
    this.itemCount = itemCount;
    return this;
  }

   /**
   * This parameter contains the number of items in additional_data_list. Note it is provided if additional_data_list is populated.  It is conditional and holds a numeric value with a maximum length of two and a minimum length of one.
   * @return itemCount
  **/
  @ApiModelProperty(example = "2", value = "This parameter contains the number of items in additional_data_list. Note it is provided if additional_data_list is populated.  It is conditional and holds a numeric value with a maximum length of two and a minimum length of one.")
  public String getItemCount() {
    return itemCount;
  }

  public void setItemCount(String itemCount) {
    this.itemCount = itemCount;
  }

  public AdditionalDataList data(Data data) {
    this.data = data;
    return this;
  }

   /**
   * Get data
   * @return data
  **/
  @ApiModelProperty(value = "")
  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AdditionalDataList additionalDataList = (AdditionalDataList) o;
    return Objects.equals(this.resourceType, additionalDataList.resourceType) &&
        Objects.equals(this.itemCount, additionalDataList.itemCount) &&
        Objects.equals(this.data, additionalDataList.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(resourceType, itemCount, data);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AdditionalDataList {\n");
    
    sb.append("    resourceType: ").append(toIndentedString(resourceType)).append("\n");
    sb.append("    itemCount: ").append(toIndentedString(itemCount)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

