/*
 * Crossborder
 * Send cross-border APIs.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apisupport@mastercard.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.client.model.Address;
import org.openapitools.client.model.GovernmentIds;

/**
 * Sender name and address information is required for a payment transfer.
 */
@ApiModel(description = "Sender name and address information is required for a payment transfer.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-05-09T06:16:48.801+03:00[Asia/Kuwait]")
public class Sender {
  public static final String SERIALIZED_NAME_FIRST_NAME = "first_name";
  @SerializedName(SERIALIZED_NAME_FIRST_NAME)
  private String firstName;

  public static final String SERIALIZED_NAME_MIDDLE_NAME = "middle_name";
  @SerializedName(SERIALIZED_NAME_MIDDLE_NAME)
  private String middleName;

  public static final String SERIALIZED_NAME_LAST_NAME = "last_name";
  @SerializedName(SERIALIZED_NAME_LAST_NAME)
  private String lastName;

  public static final String SERIALIZED_NAME_ORGANIZATION_NAME = "organization_name";
  @SerializedName(SERIALIZED_NAME_ORGANIZATION_NAME)
  private String organizationName;

  public static final String SERIALIZED_NAME_NATIONALITY = "nationality";
  @SerializedName(SERIALIZED_NAME_NATIONALITY)
  private String nationality;

  public static final String SERIALIZED_NAME_ADDRESS = "address";
  @SerializedName(SERIALIZED_NAME_ADDRESS)
  private Address address = null;

  public static final String SERIALIZED_NAME_GOVERNMENT_IDS = "government_ids";
  @SerializedName(SERIALIZED_NAME_GOVERNMENT_IDS)
  private List<GovernmentIds> governmentIds = null;

  public static final String SERIALIZED_NAME_DATE_OF_BIRTH = "date_of_birth";
  @SerializedName(SERIALIZED_NAME_DATE_OF_BIRTH)
  private String dateOfBirth;

  public Sender firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * This parameter contains the consumer’s first name for transactions where the sender is a person. For transactions where the sender is not a person, this field is not allowed.   Note that this parameter is required for transactions where the sender is a person, i.e. the value of quoterequest or paymentrequest.payment_type is P2P or P2B. Providing this with other payment types will cause an error.   It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1. 
   * @return firstName
  **/
  @ApiModelProperty(example = "JOHN", value = "This parameter contains the consumer’s first name for transactions where the sender is a person. For transactions where the sender is not a person, this field is not allowed.   Note that this parameter is required for transactions where the sender is a person, i.e. the value of quoterequest or paymentrequest.payment_type is P2P or P2B. Providing this with other payment types will cause an error.   It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1. ")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Sender middleName(String middleName) {
    this.middleName = middleName;
    return this;
  }

   /**
   * This parameter contains the consumer’s middle name for transactions where the sender is a person. For transactions where the sender is not a person, this field is not allowed.   Note that this parameter is optional for transactions where the sender is a person, i.e. the value of quoterequest or paymentrequest.payment_type is P2P or P2B. Providing this with other payment types will cause an error.     It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.  
   * @return middleName
  **/
  @ApiModelProperty(example = "Adam", value = "This parameter contains the consumer’s middle name for transactions where the sender is a person. For transactions where the sender is not a person, this field is not allowed.   Note that this parameter is optional for transactions where the sender is a person, i.e. the value of quoterequest or paymentrequest.payment_type is P2P or P2B. Providing this with other payment types will cause an error.     It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.  ")
  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public Sender lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * This parameter contains the consumer’s last name for transactions where the sender is a person. For transactions where the sender is not a person, this field is not allowed.   Note that this parameter is required for transactions where the sender is a person, i.e. the value of quoterequest or paymentrequest.payment_type is P2P, P2B. Providing this with other payment types will cause an error.  It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.  
   * @return lastName
  **/
  @ApiModelProperty(example = "SMITH", value = "This parameter contains the consumer’s last name for transactions where the sender is a person. For transactions where the sender is not a person, this field is not allowed.   Note that this parameter is required for transactions where the sender is a person, i.e. the value of quoterequest or paymentrequest.payment_type is P2P, P2B. Providing this with other payment types will cause an error.  It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.  ")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Sender organizationName(String organizationName) {
    this.organizationName = organizationName;
    return this;
  }

   /**
   * This parameter contains the organization’s name for transactions where the sender is not a person. For transactions where the sender is a person, this field is not allowed.    Note that his parameter is required for transactions where the sender is an organization, i.e. the value of quoterequest or paymentrequest.payment_type is B2B, B2P, G2P. Providing this with other payment types will cause an error.  It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.  
   * @return organizationName
  **/
  @ApiModelProperty(example = "ABC Company", value = "This parameter contains the organization’s name for transactions where the sender is not a person. For transactions where the sender is a person, this field is not allowed.    Note that his parameter is required for transactions where the sender is an organization, i.e. the value of quoterequest or paymentrequest.payment_type is B2B, B2P, G2P. Providing this with other payment types will cause an error.  It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.  ")
  public String getOrganizationName() {
    return organizationName;
  }

  public void setOrganizationName(String organizationName) {
    this.organizationName = organizationName;
  }

  public Sender nationality(String nationality) {
    this.nationality = nationality;
    return this;
  }

   /**
   * For transactions where the sender is a person, this is the sending consumer&#39;s nationality. This parameter contains the three-letter \&quot;ISO 3166-1 alpha-3\&quot; country code representing the sender’s nationality. In the case of a business or government organization, this is the registered country of the organization.   It holds a string of alphabet characters with an exact length of three.
   * @return nationality
  **/
  @ApiModelProperty(example = "USA", value = "For transactions where the sender is a person, this is the sending consumer's nationality. This parameter contains the three-letter \"ISO 3166-1 alpha-3\" country code representing the sender’s nationality. In the case of a business or government organization, this is the registered country of the organization.   It holds a string of alphabet characters with an exact length of three.")
  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public Sender address(Address address) {
    this.address = address;
    return this;
  }

   /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(value = "")
  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Sender governmentIds(List<GovernmentIds> governmentIds) {
    this.governmentIds = governmentIds;
    return this;
  }

  public Sender addGovernmentIdsItem(GovernmentIds governmentIdsItem) {
    if (this.governmentIds == null) {
      this.governmentIds = new ArrayList<GovernmentIds>();
    }
    this.governmentIds.add(governmentIdsItem);
    return this;
  }

   /**
   * List of sender government ids. Note: There can be 0 to many of these fields required and used for each transaction.
   * @return governmentIds
  **/
  @ApiModelProperty(value = "List of sender government ids. Note: There can be 0 to many of these fields required and used for each transaction.")
  public List<GovernmentIds> getGovernmentIds() {
    return governmentIds;
  }

  public void setGovernmentIds(List<GovernmentIds> governmentIds) {
    this.governmentIds = governmentIds;
  }

  public Sender dateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
    return this;
  }

   /**
   * Required field when the sender is a person, conditional field when sender is not a person.  In case of Person: Sender’s Date of Birth   In case of Business or Government Organization: Date of Establishment or Incorporation  It is a conditional parameter that holds an “ISO 8601” date of the format YYYY-MM-DD.
   * @return dateOfBirth
  **/
  @ApiModelProperty(example = "1985-06-24", value = "Required field when the sender is a person, conditional field when sender is not a person.  In case of Person: Sender’s Date of Birth   In case of Business or Government Organization: Date of Establishment or Incorporation  It is a conditional parameter that holds an “ISO 8601” date of the format YYYY-MM-DD.")
  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Sender sender = (Sender) o;
    return Objects.equals(this.firstName, sender.firstName) &&
        Objects.equals(this.middleName, sender.middleName) &&
        Objects.equals(this.lastName, sender.lastName) &&
        Objects.equals(this.organizationName, sender.organizationName) &&
        Objects.equals(this.nationality, sender.nationality) &&
        Objects.equals(this.address, sender.address) &&
        Objects.equals(this.governmentIds, sender.governmentIds) &&
        Objects.equals(this.dateOfBirth, sender.dateOfBirth);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, middleName, lastName, organizationName, nationality, address, governmentIds, dateOfBirth);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Sender {\n");
    
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    organizationName: ").append(toIndentedString(organizationName)).append("\n");
    sb.append("    nationality: ").append(toIndentedString(nationality)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    governmentIds: ").append(toIndentedString(governmentIds)).append("\n");
    sb.append("    dateOfBirth: ").append(toIndentedString(dateOfBirth)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

