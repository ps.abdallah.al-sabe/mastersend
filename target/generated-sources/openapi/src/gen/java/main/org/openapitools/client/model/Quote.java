/*
 * Crossborder
 * Send cross-border APIs.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apisupport@mastercard.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.openapitools.client.model.Proposals;

/**
 * Response details
 */
@ApiModel(description = "Response details")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-05-09T06:16:48.801+03:00[Asia/Kuwait]")
public class Quote {
  public static final String SERIALIZED_NAME_TRANSACTION_REFERENCE = "transaction_reference";
  @SerializedName(SERIALIZED_NAME_TRANSACTION_REFERENCE)
  private String transactionReference;

  public static final String SERIALIZED_NAME_PAYMENT_TYPE = "payment_type";
  @SerializedName(SERIALIZED_NAME_PAYMENT_TYPE)
  private String paymentType;

  public static final String SERIALIZED_NAME_PROPOSALS = "proposals";
  @SerializedName(SERIALIZED_NAME_PROPOSALS)
  private Proposals proposals = null;

  public Quote transactionReference(String transactionReference) {
    this.transactionReference = transactionReference;
    return this;
  }

   /**
   * This parameter contains the client provided unique reference number.   It holds alphanumeric special characters with a maximum length of 40 and a minimum length of 1. 
   * @return transactionReference
  **/
  @ApiModelProperty(example = "0999999034810154901", value = "This parameter contains the client provided unique reference number.   It holds alphanumeric special characters with a maximum length of 40 and a minimum length of 1. ")
  public String getTransactionReference() {
    return transactionReference;
  }

  public void setTransactionReference(String transactionReference) {
    this.transactionReference = transactionReference;
  }

  public Quote paymentType(String paymentType) {
    this.paymentType = paymentType;
    return this;
  }

   /**
   * This parameter contains a three-digit code for the type of transaction that is being submitted.  Available types and their uses are provided below:  B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.  B2B: Business to Business- A transfer of funds from one business to another.  G2P: Government to Person - A disbursement of funds from a government agency to a private individual person&#39;s account.  P2P: Person to Person - A transfer of funds from one private individual person&#39;s account to another private individual person&#39;s account.  P2B: Person to Business - A payment by an individual person to a business  It holds a string of alphabet characters with an exact length of three. 
   * @return paymentType
  **/
  @ApiModelProperty(example = "B2B", value = "This parameter contains a three-digit code for the type of transaction that is being submitted.  Available types and their uses are provided below:  B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.  B2B: Business to Business- A transfer of funds from one business to another.  G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account.  P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.  P2B: Person to Business - A payment by an individual person to a business  It holds a string of alphabet characters with an exact length of three. ")
  public String getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(String paymentType) {
    this.paymentType = paymentType;
  }

  public Quote proposals(Proposals proposals) {
    this.proposals = proposals;
    return this;
  }

   /**
   * Get proposals
   * @return proposals
  **/
  @ApiModelProperty(value = "")
  public Proposals getProposals() {
    return proposals;
  }

  public void setProposals(Proposals proposals) {
    this.proposals = proposals;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Quote quote = (Quote) o;
    return Objects.equals(this.transactionReference, quote.transactionReference) &&
        Objects.equals(this.paymentType, quote.paymentType) &&
        Objects.equals(this.proposals, quote.proposals);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionReference, paymentType, proposals);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Quote {\n");
    
    sb.append("    transactionReference: ").append(toIndentedString(transactionReference)).append("\n");
    sb.append("    paymentType: ").append(toIndentedString(paymentType)).append("\n");
    sb.append("    proposals: ").append(toIndentedString(proposals)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

