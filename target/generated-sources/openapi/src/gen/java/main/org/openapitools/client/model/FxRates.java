/*
 * Crossborder
 * Send cross-border APIs.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apisupport@mastercard.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.client.model.FxRate;

/**
 * See child attributes
 */
@ApiModel(description = "See child attributes")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-05-09T06:16:48.801+03:00[Asia/Kuwait]")
public class FxRates {
  public static final String SERIALIZED_NAME_RATE = "rate";
  @SerializedName(SERIALIZED_NAME_RATE)
  private List<FxRate> rate = null;

  public FxRates rate(List<FxRate> rate) {
    this.rate = rate;
    return this;
  }

  public FxRates addRateItem(FxRate rateItem) {
    if (this.rate == null) {
      this.rate = new ArrayList<FxRate>();
    }
    this.rate.add(rateItem);
    return this;
  }

   /**
   * See child attributes
   * @return rate
  **/
  @ApiModelProperty(value = "See child attributes")
  public List<FxRate> getRate() {
    return rate;
  }

  public void setRate(List<FxRate> rate) {
    this.rate = rate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FxRates fxRates = (FxRates) o;
    return Objects.equals(this.rate, fxRates.rate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rate);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FxRates {\n");
    
    sb.append("    rate: ").append(toIndentedString(rate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

