/*
 * Crossborder
 * Send cross-border APIs.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apisupport@mastercard.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.api;

import org.openapitools.client.ApiCallback;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.ApiResponse;
import org.openapitools.client.Configuration;
import org.openapitools.client.Pair;
import org.openapitools.client.ProgressRequestBody;
import org.openapitools.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import org.openapitools.client.model.EventWrapper;
import org.openapitools.client.model.RatesWrapper;
import org.openapitools.client.model.StatusWrapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CardedRateApi {
    private ApiClient apiClient;

    public CardedRateApi() {
        this(Configuration.getDefaultApiClient());
    }

    public CardedRateApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for cardedPUSH
     * @param eventWrapper See child attributes (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call cardedPUSHCall(EventWrapper eventWrapper, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = eventWrapper;

        // create path and map variables
        String localVarPath = "/callbackpath";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if (progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call cardedPUSHValidateBeforeCall(EventWrapper eventWrapper, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = cardedPUSHCall(eventWrapper, progressListener, progressRequestListener);
        return call;

    }

    /**
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  
     * @param eventWrapper See child attributes (optional)
     * @return StatusWrapper
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public StatusWrapper cardedPUSH(EventWrapper eventWrapper) throws ApiException {
        ApiResponse<StatusWrapper> resp = cardedPUSHWithHttpInfo(eventWrapper);
        return resp.getData();
    }

    /**
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  
     * @param eventWrapper See child attributes (optional)
     * @return ApiResponse&lt;StatusWrapper&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<StatusWrapper> cardedPUSHWithHttpInfo(EventWrapper eventWrapper) throws ApiException {
        com.squareup.okhttp.Call call = cardedPUSHValidateBeforeCall(eventWrapper, null, null);
        Type localVarReturnType = new TypeToken<StatusWrapper>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.   (asynchronously)
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Push, once coded to, will provide the ability for Mastercard Send to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI. Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day. It is a Mastercard Send best practice for any customer who codes to the Push API to also code to the Pull API so that the pull API can be used as a backup in case FX rates are not received for any reason.  
     * @param eventWrapper See child attributes (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call cardedPUSHAsync(EventWrapper eventWrapper, final ApiCallback<StatusWrapper> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = cardedPUSHValidateBeforeCall(eventWrapper, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<StatusWrapper>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getFxRates
     * @param partnerId Path Param - This is the Assigned Provider Id. Details- This parameter holds a string of alphanumeric special characters with a max length of 35. (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getFxRatesCall(String partnerId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = new Object();

        // create path and map variables
        String localVarPath = "/send/v1/partners/{partner-id}/crossborder/rates"
            .replaceAll("\\{" + "partner-id" + "\\}", apiClient.escapeString(partnerId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if (progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getFxRatesValidateBeforeCall(String partnerId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'partnerId' is set
        if (partnerId == null) {
            throw new ApiException("Missing the required parameter 'partnerId' when calling getFxRates(Async)");
        }
        

        com.squareup.okhttp.Call call = getFxRatesCall(partnerId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.
     * @param partnerId Path Param - This is the Assigned Provider Id. Details- This parameter holds a string of alphanumeric special characters with a max length of 35. (required)
     * @return RatesWrapper
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public RatesWrapper getFxRates(String partnerId) throws ApiException {
        ApiResponse<RatesWrapper> resp = getFxRatesWithHttpInfo(partnerId);
        return resp.getData();
    }

    /**
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.
     * @param partnerId Path Param - This is the Assigned Provider Id. Details- This parameter holds a string of alphanumeric special characters with a max length of 35. (required)
     * @return ApiResponse&lt;RatesWrapper&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<RatesWrapper> getFxRatesWithHttpInfo(String partnerId) throws ApiException {
        com.squareup.okhttp.Call call = getFxRatesValidateBeforeCall(partnerId, null, null);
        Type localVarReturnType = new TypeToken<RatesWrapper>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send. (asynchronously)
     * The Carded Rate APIs are part of an opt-in service that provides FX rates for currency pairs supported by an OI who has opted in to participate in the service. The Carded Rate APIs will provide dealable FX rates, rate IDs, effective times, as well as other information. OIs who have not opted into this service but code to these APIs will not receive rates (Push API) and will receive an error by the system upon submitting a request (Pull API). Refer to the Mastercard Send Cross-Border Product guide for further details on the service.  The FX Rate API - Pull, if used as the primary mechanism to retrieve FX rates, will require the OI to create a scheduler that will call this API based on refresh times per currency pair provided by Mastercard Send.
     * @param partnerId Path Param - This is the Assigned Provider Id. Details- This parameter holds a string of alphanumeric special characters with a max length of 35. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getFxRatesAsync(String partnerId, final ApiCallback<RatesWrapper> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getFxRatesValidateBeforeCall(partnerId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<RatesWrapper>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
