FROM openjdk:8
COPY ./target/*.jar app/usr/app.jar/
ENV spring.profiles.active=h2
EXPOSE 8080
ENTRYPOINT java -jar -Dspring.profiles.active=h2 /usr/app/app.jar

